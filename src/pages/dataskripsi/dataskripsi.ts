import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';


/**
 * Generated class for the DataskripsiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dataskripsi',
  templateUrl: 'dataskripsi.html',
})
export class DataskripsiPage {
  nama:string;
  nim:string;
  judul:string;
  class:string;
  pembimbing: string[];
  // nama1:string;
  // nama2:string;
  linpro:string;
  x:boolean;
  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.nama = this.navParams.get("nama");
    this.nim = this.navParams.get("nim");
    this.class = this.navParams.get("kelas");
    this.judul = this.navParams.get("judul");
    this.pembimbing = this.navParams.get("pembimbing");
    // this.nama2 = this.navParams.get("nama2");
    this.x = this.navParams.get("linpro");
    console.log(this.x);
    if(this.x==true){
      this.linpro= "Ya";
    }
    else{
      this.linpro= "Tidak";
    }
    console.log('ionViewDidLoad DataskripsiPage');
  }

  closeModal(remove:boolean){
    this.viewCtrl.dismiss(remove);
    console.log(remove);
  }

}
