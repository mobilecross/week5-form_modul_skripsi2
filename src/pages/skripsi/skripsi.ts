import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController  } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '../../../node_modules/@angular/forms';
import { DataskripsiPage } from '../dataskripsi/dataskripsi';
/**
 * Generated class for the SkripsiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-skripsi',
  templateUrl: 'skripsi.html',
})
export class SkripsiPage implements OnInit{

  isChecked=false;
  form : FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private modalCtrl:ModalController,
    private _FB: FormBuilder) {
  }

  ngOnInit() {
    console.log('ionViewDidLoad SkripsiPage');
    this.initTechnologyFields();
   //console.log(this.form);
  }

  changeValue($event){
    this.isChecked = !this.isChecked; // I am assuming you want to switch between checking and unchecking while clicking on radio.
  }

  submitSkripsi(){
    console.log(this.isChecked);
    console.log(this.form.value.nama);
    //console.log(f.value.nama);
    let modal = this.modalCtrl.create(DataskripsiPage,{nama:this.form.value.nama, nim:this.form.value.nim, judul: this.form.value.judul, kelas:this.form.value.optClass,pembimbing:this.form.value.pembimbing,linpro:this.isChecked});
    modal.present();
    
  }

  initTechnologyFields(){
   this.form = new FormGroup({
    nama       	  : new FormControl(null , Validators.required),
    nim       	  : new FormControl(null , Validators.required),
    judul       	: new FormControl(null , Validators.required),
    optClass      : new FormControl(null , Validators.required),
    pembimbing    : this._FB.array([this.initTechnology()])//,
    //lintas        : new FormControl(false , Validators.required)
   });
}

initTechnology(){
  return this._FB.group({
    name : ['', Validators.required]
 });
}

addNewInputField()
{
   const control = <FormArray>this.form.controls.pembimbing;
   control.push(this.initTechnology());
}

removeInputField(i : number)
   {
      const control = <FormArray>this.form.controls.pembimbing;
      control.removeAt(i);
   }

}
